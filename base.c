//Alex Omar Toro Sepulveda, 19.941.255-8
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
/*float aprovados(estudiante curso[],int i){

}*/

float desvStd(estudiante curso[],int i){
	float n,dif_al_cuadrado_proyecto1,dif_al_cuadrado_proyecto2,dif_al_cuadrado_proyecto3,dif_al_cuadrado_control1,dif_al_cuadrado_control2,dif_al_cuadrado_control3,dif_al_cuadrado_control4,dif_al_cuadrado_control5,dif_al_cuadrado_control6,suma,raiz;
	n =curso[i].prom;
	dif_al_cuadrado_proyecto1 = (curso[i].asig_1.proy1 - n)*(curso[i].asig_1.proy1 - n);
	dif_al_cuadrado_proyecto2 = (curso[i].asig_1.proy2 - n)*(curso[i].asig_1.proy2 - n); // les saco el elevado a 2 sin pow
	dif_al_cuadrado_proyecto3 = (curso[i].asig_1.proy3 - n)*(curso[i].asig_1.proy3 - n);
	dif_al_cuadrado_control1 = (curso[i].asig_1.cont1 - n)*(curso[i].asig_1.cont1 - n);
	dif_al_cuadrado_control2 = (curso[i].asig_1.cont2 - n)*(curso[i].asig_1.cont2 - n);
	dif_al_cuadrado_control3 = (curso[i].asig_1.cont3 - n)*(curso[i].asig_1.cont3 - n);
	dif_al_cuadrado_control4 = (curso[i].asig_1.cont4 - n)*(curso[i].asig_1.cont4 - n);
	dif_al_cuadrado_control5 = (curso[i].asig_1.cont5 - n)*(curso[i].asig_1.cont5 - n);
	dif_al_cuadrado_control6 = (curso[i].asig_1.cont6 - n)*(curso[i].asig_1.cont6 - n);
	suma = ((dif_al_cuadrado_proyecto1 + dif_al_cuadrado_proyecto2 + dif_al_cuadrado_proyecto3 + dif_al_cuadrado_control1 + dif_al_cuadrado_control2 + dif_al_cuadrado_control3 + dif_al_cuadrado_control4 + dif_al_cuadrado_control5 + dif_al_cuadrado_control6)/8);
	return suma;// los elevados se suman y los hace retornar
}

float menor(int i,estudiante curso[]){
	FILE *reprobado;
	if((reprobado=fopen("reprobado.txt","w"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0);
	}
	else{
		printf("%s %s %s: %.1f",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
		fprintf(reprobado, "%s %s %s: %.1f",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
	}
	fclose(reprobado);
	return 0.0;
}

float mayor(int i,estudiante curso[]){
	FILE *aprobado;
	if((aprobado=fopen("aprobado.txt","w"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0);
	}
	else{
		printf("%s %s %s: %.1f",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
		fprintf(aprobado, "%s %s %s: %.1f",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
	}
	fclose(aprobado);
	return 0.0;
}

void registroCurso(estudiante curso[],char notas[],char path[]){
	//debe registrar las calificaciones 
	FILE *file; //se crean dos file para nota.txt y para estudiante, una para escribir y otra para que lo lea respectivamente y se guardan en una variable que se podran usar en otras funciones
	FILE *file2;
	file2=fopen(notas,"w");
	int i = 0;
	printf("Notas:\n");
	if((file=fopen(path,"r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0); 
	}
	else{
		while (feof(file) == 0) {
			if(i == 24)break; //este if espara que la basura que aparece en el final del archivo no aparesca y me rompa el codigo.
			fscanf(file,"%s %s %s", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			printf("Ingrese las notas del alumno: %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			printf("Ingrese la nota del proyecto 1: ");
			scanf("%f",&curso[i].asig_1.proy1);  // hago que ingresen las notas de los proyectos y controles
			printf("Ingrese la nota del proyecto 2: ");
			scanf("%f",&curso[i].asig_1.proy2);
			printf("Ingrese la nota del proyecto 3: ");
			scanf("%f",&curso[i].asig_1.proy3);
			printf("Ingrese la nota del control 1: ");
			scanf("%f",&curso[i].asig_1.cont1);
			printf("Ingrese la nota del control 2: ");
			scanf("%f",&curso[i].asig_1.cont2);
			printf("Ingrese la nota del control 3: ");
			scanf("%f",&curso[i].asig_1.cont3);
			printf("Ingrese la nota del control 4: ");
			scanf("%f",&curso[i].asig_1.cont4);
			printf("Ingrese la nota del control 5: ");
			scanf("%f",&curso[i].asig_1.cont5);
			printf("Ingrese la nota del control 6: ");
			scanf("%f",&curso[i].asig_1.cont6);
			if(file2 == NULL){
				printf("\nerror al arbir el archivo");
				exit(0);
			}
			else{
				printf("%d) %s %s %s %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n\n",i+1,curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3, curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6);
				fprintf(file2,"%s %s %s %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3, curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6);
			} // Lo imprimo en el archivo notas txt y lo muestro por pantalla.
			i++;

		}
		fclose(file); // se cierran los archivos
		fclose(file2);
	} 
	

}

void clasificarEstudiantes(char path[], estudiante curso[]){
	float rep,apro;
	int i;
	while(i < 24){
		if(curso[i].prom <= 3.9){
			rep=menor(i,curso); // se supone que iria a las notas menores a 4 y crear un archivos con estas
		}
		else{
			apro=mayor(i,curso); // lo mismo pero con las mayores o iguales a 4.
		}
		i++; // contador
	}
	printf("TEST"); 
}


void metricasEstudiantes(estudiante curso[]){
	int i=0,j=0,k=0;
	float max = 0; // se asigna como maximo = 0 para que el que entre se el nuevo maximo
	float min = 7; // se asigna como minimo = 7 para que el que entre se el nuevo minimo
	float desv;
	FILE *file;
	if((file=fopen("estudiantes.txt","r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0); 
	}
	else{
		while (feof(file) == 0) {
			if(i == 24)break;
			fscanf(file,"%s %s %s", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			curso[i].prom = curso[i].asig_1.proy1*0.2 +curso[i].asig_1.proy2*0.2+curso[i].asig_1.proy3*0.3+curso[i].asig_1.cont1*0.05+curso[i].asig_1.cont2*0.05+curso[i].asig_1.cont3*0.05+curso[i].asig_1.cont4*0.05+curso[i].asig_1.cont5*0.05+curso[i].asig_1.cont6*0.05;
			// se saca el promedio al estilo universitario
			printf("El promedio de %s %s %s %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].prom); // lo muestra por pantalla
			desv=desvStd(curso,i); // se dirije a la funcion de desviacion estandar
			printf("Y su desviacion estandar es: %.1f\n\n",pow(desv,0.5));// muestra los retornado por desvStd pero sacandole la raiz usando pow
			if(min >= curso[i].prom){ // este if sirve para diferenciar el promedio mas bajo
				min = curso[i].prom;
				j=i;
			}
			if(max <= curso[i].prom){ // este if es para diferenciar el promedio mas alto
				max = curso[i].prom;
				k=i;
			}

			i++;
		}
		printf("La nota mas baja es de:\n-%s %s %s y su promedio fue de: %.1f\n", curso[j].nombre,curso[j].apellidoP, curso[j].apellidoM,min);
		printf("y la mas alta es de:\n-%s %s %s y su promedio fue de: %.1f\n", curso[k].nombre,curso[k].apellidoP, curso[k].apellidoM,max); // esprintean la nota mas baja y mas alta

		
	}	
	 
}


void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso,"notas.txt","estudiantes.txt");// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
     	case 5: clasificarEstudiantes("destino", curso); // clasi
          break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}
